package com.agiletestingalliance;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.Test;
import org.mockito.Mockito;

public class UsefulnessTest extends Mockito{
	
	@Test
	public void testUsefulness() throws Exception {
		String str = new Usefulness().desc();
		assertEquals("Problem with dur function", 411, str.length());
	}
}
