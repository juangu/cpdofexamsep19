package com.agiletestingalliance;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.Test;
import org.mockito.Mockito;

public class DurationTest extends Mockito{
	
	@Test
	public void testDuration() throws Exception {
		String str = new Duration().dur();
		assertEquals("Problem with dur function", 261, str.length());
	}
}
