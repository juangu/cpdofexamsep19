package com.agiletestingalliance;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.Test;
import org.mockito.Mockito;

public class AboutCPDOFTest extends Mockito{
	
	@Test
	public void testAboutCPDOF() throws Exception {
		String str = new AboutCPDOF().desc();
		assertEquals("Problem with dur function", 428, str.length());
	}
}
